#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate log;

extern crate serde_json;
extern crate env_logger;
extern crate chrono;
extern crate md5;
extern crate reqwest;
extern crate rss;
extern crate toml;


use chrono::prelude::*;
use rss::{Channel, Item, ChannelBuilder, ImageBuilder, ItemBuilder, EnclosureBuilder};
use std::fs;
use std::path::Path;
use std::process::{Command, Stdio};
use std::io::{Write, Read};
use std::rc::Rc;
use std::time::{SystemTime, UNIX_EPOCH};

fn run_command(cmd_str: &str) -> Result<String, String> {
    let mut cmd = Command::new("sh");
    cmd.arg("-c");
    cmd.arg(cmd_str);
    cmd.stdout(Stdio::piped());
    cmd.status()
        .map_err(|e| { format!("Command `{:?}` failed: {:#?}", cmd_str, e)})
        .and_then(|ok| { 
            if ok.success() { 
                let output = cmd.output().map_err(|e| { 
                    format!("Failed to read output of command `{}`: {:#?}", cmd_str, e) })?;
                String::from_utf8(output.stdout).or(Err(format!("Output of command `{}` not UTF8", cmd_str)))
            } 
            else { 
                Err(format!("Command `{:?}` exited with {:?}", cmd_str, ok))
            }
        })
}

fn ffmpeg(input: &str, output: &str, bitrate: usize, ffmpeg_flags: &str) -> String {
    format!(
        "ffmpeg -i {input} -b:a {bitrate} {ffmpeg_flags} {output}",
        ffmpeg_flags = ffmpeg_flags,
        input = input,
        output = output,
        bitrate = bitrate
    )
}

const AUDIO_FOLDER: &'static str = "audio";
const AUDIO_BRANCH: &'static str = "audio";
const TEMPORARY_AUDIO_FOLDER: &'static str = "audio_temp";
const RSS_FOLDER: &'static str = "rss";
const COMMIT_MESSAGE: &'static str = "Automatic commit";

#[derive(Debug, Default, Clone)]
struct SmallItem {
    enclosure: String,
    title: String,
    description: String,
    link: String,
    pub_date: String,

    author: Option<String>,
    guid: Option<String>,

    file: String,

    conf: Rc<Conf>,
}

#[derive(Debug, Default)]
struct SmallChannel {
    link: String,
    title: String,
    description: String,

    image: Option<String>,
    copyright: Option<String>,

    items: Vec<SmallItem>,

    conf: Rc<Conf>
}

#[derive(Serialize)]
struct JsonChannel {
    title: String,
    url: String,
    home: String,
}

impl From<SmallChannel> for JsonChannel {
    fn from(ch: SmallChannel) -> JsonChannel {
        JsonChannel {
            title: ch.title.clone(),
            home: ch.link.clone(),
            url: format!("{}/{}?env=dev", ch.conf.cdn_link(), ch.rss_file())
        }
    }
}

fn own(s: Option<&str>) -> Option<String> {
    s.and_then(|t| Some(String::from(t)))
}

impl SmallItem {
    fn from_rss(item: &Item, channel_link: &str, channel_title: &str, conf: Rc<Conf>) -> Option<SmallItem> {
        let description = item
            .itunes_ext()
            .and_then(|e| e.summary())
            .or(item.description());

        let enclosure_valid = match item.enclosure() {
            None => false,
            Some(enc) => enc.url().ends_with("mp3"),
        };

        if item.title().is_none() || description.is_none() || !enclosure_valid {
            return None;
        }

        let description = description.unwrap();
        let description_len = description.chars().count();
        let descr_str = if description_len > conf.description_size {
            format!("{}...", description.chars().take(conf.description_size - 3).collect::<String>())
        } else {
            String::from(description)
        };

        let date_str_opt = item.pub_date();
        if date_str_opt.is_none() {
            return None;
        }

        let date_str = date_str_opt.unwrap().replace("-0000", "+0000");
        let date = DateTime::parse_from_rfc2822(&date_str);
        if date.is_err() {
            warn!("Bad time: {}", date_str);
            return None;
        }

        let date = date.unwrap();
        let days_since = Utc::now().signed_duration_since(date).num_days();
        if days_since > conf.max_age {
            return None;
        }

        let enclosure = String::from(item.enclosure().unwrap().url());
        let to_hash = format!("{}:{}", channel_title, &enclosure);
        let digest = md5::compute(to_hash);
        let file = format!("{:x}", digest);

        Some(SmallItem {
            enclosure: enclosure,
            title: own(item.title()).unwrap(),
            description: descr_str,
            link: item.link().unwrap_or(channel_link).to_owned(),
            author: own(item.author().or(item.itunes_ext().and_then(|e| e.author()))),
            pub_date: String::from(date_str),
            guid: own(item.guid().and_then(|g| Some(g.value()))),
            file: file,
            conf: conf.clone()
        })
    }
    fn temporary_file(&self) -> String {
        format!("{}/{}.mp3", TEMPORARY_AUDIO_FOLDER, self.file)
    }

    fn transcoded_file(&self) -> String {
        format!("{}/{}.ogg", AUDIO_FOLDER, self.file)
    }

    fn file_url(&self) -> String {
        format!("{}/{}/{}.ogg", self.conf.cdn_link(), AUDIO_FOLDER, self.file)
    }

    fn download_file(&self) -> Option<()> {
        let response_result = reqwest::get(&self.enclosure);
        if let Err(_) = &response_result {
            warn!("Fail obtaining url: {}", self.enclosure);
            return None;
        }

        let mut response = response_result.unwrap();
        if !response.status().is_success() {
            warn!(
                "Bad url response status for {}: {}",
                self.enclosure,
                response.status()
            );
            return None;
        }

        let filename = self.temporary_file();
        let file_res = fs::File::create(&filename);
        if file_res.is_err() {
            warn!("Unable to create file: {}", filename);
            return None;
        }

        let mut file = file_res.unwrap();
        match response.copy_to(&mut file) {
            Err(_) => {
                let _ = fs::remove_file(&filename);
                warn!("Unable to write data to file: {}", filename);
                None
            }
            _ => Some(()),
        }
    }

    fn transcode(&self) -> Result<(), String> {
        let input_file = self.temporary_file();
        let output_file = self.transcoded_file();
        let command = ffmpeg(&input_file, &output_file, self.conf.bitrate, &self.conf.ffmpeg_flags);
        run_command(&command)?;
        Ok(())
    }

    fn is_downloaded(&self) -> bool {
        let file = self.temporary_file();
        let path = Path::new(&file);
        Path::exists(&path)
    }

    fn is_transcoded(&self) -> bool {
        let file = self.transcoded_file();
        let path = Path::new(&file);
        Path::exists(&path)
    }

    fn as_item(&self) -> Option<Item> {
        let enclosure = EnclosureBuilder::default()
        .url(self.file_url())
        .mime_type("audio/ogg").build().ok();

        ItemBuilder::default()
        .title(self.title.to_owned())
        .description(self.description.to_owned())
        .link(self.link.to_owned())
        .author(self.author.to_owned())
        .enclosure(enclosure)
        .pub_date(Some(self.pub_date.to_owned()))
        .build().ok()
    }
}

impl SmallChannel {
    fn from_rss(rss_channel: &Channel, conf: Rc<Conf>) -> SmallChannel {
        let channel_link = rss_channel.link();
        let channel_name = rss_channel.title();

        let make_item = |item: &Item| SmallItem::from_rss(item, &channel_link, &channel_name, conf.clone());
        let items = rss_channel
            .items()
            .iter()
            .filter_map(make_item)
            .collect::<Vec<SmallItem>>();

        let image = rss_channel.image()
            .and_then(|img| Some(img.url()))
            .or_else(|| {
                rss_channel.itunes_ext().and_then(
                    |it| it.image()
                )
            });

        SmallChannel {
            link: String::from(channel_link),
            title: String::from(channel_name),
            description: String::from(rss_channel.description()),
            image: own(image),
            copyright: own(rss_channel.copyright()),
            items: items,
            conf: conf.clone(),
        }
    }

    fn rss_file(&self) -> String {
        let title = self.title.replace(" ", "-");
        format!("{}/{}.rss", RSS_FOLDER, title)
    }

    fn as_channel(&self) -> Option<Channel> {
        let image = self.image.as_ref().and_then(|img| { Some(ImageBuilder::default().url(img.to_owned()).build().unwrap()) });
        let items = self.items.iter().filter_map(|item| { item.as_item() }).collect::<Vec<Item>>();

        let rss = ChannelBuilder::default()
        .title(self.title.to_owned())
        .link(self.link.to_owned())
        .description(self.description.to_owned())
        .copyright(self.copyright.to_owned())
        .items(items)
        .pub_date(Some(Local::now().to_string()))
        .image(image).build();

        match rss {
            Err(e) => {
                warn!("Bad rss: {}: {}", self.title, e);
                None
            }
            Ok(channel) => Some(channel)
        }
    }

    fn write_to_disk(&self) -> Option<()> {
        match self.as_channel() {
            None => None,
            Some(channel) => {
                let filename = self.rss_file();
                match fs::File::create(&filename) {
                    Ok(file) => channel.write_to(file).ok().and(Some(())),
                    _ => {
                        warn!("Could not create file for {}: {}", self.title, filename);
                        None
                    }
                }
            }
        }
    }
}

fn cd_to_git_folder(conf: Rc<Conf>) -> Result<(), String> {
    let gitrepofolder = Path::new(&conf.git_repo_path);
    if !gitrepofolder.exists() {
        fs::create_dir(gitrepofolder).or(Err(String::from("Could not crate git repository!")))?;
    }

    let mut gitfile = gitrepofolder.to_path_buf();
    gitfile.push(".git");
    let gitinitialized = gitfile.as_path().exists();

    std::env::set_current_dir(&gitrepofolder).or(Err(String::from("Cannot `cd` to git repo folder")))?;
    if !gitinitialized {
        init_git(conf.clone())?;
    }

    run_command(&format!("git checkout {}", AUDIO_BRANCH))?;
    Ok(())
}

fn git_add() -> Result<(), String> {
    run_command(&format!("git add {}", AUDIO_FOLDER))?;
    run_command(&format!("git add {}", RSS_FOLDER))?;
    run_command("git add podcasts.json")?;
    Ok(())
}

fn git(conf: Rc<Conf>) -> Result<(), String> {
    if conf.disable_upload {
        info!("Skipping git stuff");
        Ok(())
    }
    else {
        info!("Adding audio files");
        git_add()?;
        info!("Commiting audio files");
        run_command(&format!("git commit -m '{}'", COMMIT_MESSAGE))?;
        info!("Pushing forcefully");
        run_command(&format!("git push -f -u origin {}", AUDIO_BRANCH))?;
        Ok(())
    }
}

fn git_clean_history() -> Result<(), String> {
    // https://stackoverflow.com/a/26000395
    info!("Cleaning git history!");
    run_command("git checkout --orphan cleaned_audio")?;
    git_add()?;
    run_command("git commit -m 'cleaned branch'")?;
    run_command(&format!("git branch -D {}", AUDIO_BRANCH))?;
    run_command(&format!("git branch -m {}", AUDIO_BRANCH))?;
    Ok(())
}

fn init_git(conf: Rc<Conf>) -> Result<(), String> {
    info!("initializing git");
    run_command("git init .")?;

    info!("Creating index.html");
    let conf = &conf.clone();

    let filename = format!("{}/index.html", conf.static_dir());
    let mut file = fs::File::open(&filename).map_err(|e| { format!("OPEN {} {:?}", filename, e) })?;
    let mut contents = String::new();
    file.read_to_string(&mut contents).map_err(|e| { format!("READ {} {:?}", filename, e)})?;

    let podcast_link = format!("{}/{}?env=dev", conf.cdn_link(), "podcasts.json");
    let contents = contents.replace("podcasts.json", &podcast_link);

    let mut file = fs::File::create("index.html").map_err(|e| { format!("OPEN index.html {:?}", e)})?;
    file.write_all(contents.as_bytes()).map_err(|e| { format!("WRITE index.html {:?}", e)})?;

    info!("Add and commit");
    run_command("git add .")?;
    run_command("git commit -m 'initial commit'")?;

    info!("adding remote");
    run_command(&format!("git remote add origin git@github.com:{}/{}.git", conf.github_username, conf.github_repo))?;

    info!("Push to origin/master");
    run_command("git push -u origin master")?;

    info!("Make audio branch");
    run_command(&format!("git branch {}", AUDIO_BRANCH))?;
    run_command(&format!("git checkout {}", AUDIO_BRANCH))?;

    fs::create_dir(AUDIO_FOLDER).or(Err(String::from("Creating AUDIO_FOLDER in repo")))?;
    fs::create_dir(TEMPORARY_AUDIO_FOLDER).or(Err(String::from("Creating AUDIO_FOLDER in repo")))?;
    fs::create_dir(RSS_FOLDER).or(Err(String::from("Creating AUDIO_FOLDER in repo")))?;

    Ok(())
}


#[derive(Clone, Serialize, Deserialize, Default, Debug)]
struct Conf {
    description_size: usize,
    rss_urls: Vec<String>,
    max_age: i64,
    git_repo_path: String,
    disable_upload: bool,
    ffmpeg_flags: String,
    github_username: String,
    github_repo: String,
    clean_git_days: u64,
    bitrate: usize,

    base_directory: Option<String>,
}

impl Conf {
    fn open() -> Result<Conf, String> {
        let filename = &Conf::config_file();
        let mut file = fs::File::open(filename).map_err(|e| { format!("OPEN {} {:?}", filename, e) })?;
        let mut contents = String::new();
        file.read_to_string(&mut contents).map_err(|e| { format!("READ {} {:?}", filename, e)})?;
        toml::from_str(&contents).map_err(|e| { format!("PARSE {} {:?}", filename, e)})
    }

    fn config_file() -> String {
        std::env::args().nth(1).unwrap_or(String::from("conf.toml"))
    }

    fn cdn_link(&self) -> String {
        format!("https://cdn.staticaly.com/gh/{}/{}/{}", 
            self.github_username, self.github_repo, AUDIO_BRANCH)
    }

    fn static_dir(&self) -> String {
        let dir: &str = self.base_directory.as_ref().expect("Base directory should be set here");
        format!("{}/static", dir)
    }
}

fn clean_unused_audio_files(keep: &[String]) -> Result<(), String> {
    let paths = fs::read_dir(AUDIO_FOLDER).or(Err(String::from("Can not list files in AUDIO_FOLDER")))?;

    for p in paths {
        let path = p.map_err(|e| { format!("Cannot load path in AUDIO_FOLDER: {:?}", e) })?.path();
        let path_str = format!("{}", path.display());
        if !keep.contains(&path_str) {
            info!("Removing: {}", path_str);
            fs::remove_file(path).map_err(|e| { format!("Cannot remove file {}: {:?}", path_str, e) })?;
        }
    }

    Ok(())
}

fn try_clean_git(conf: Rc<Conf>) -> Result<(), String> {
    let time_str_output = run_command("git log --pretty=format:%at --reverse")?;
    let time_str = time_str_output.lines().next();
    if time_str.is_none() {
        return Ok(())
    }
    let first_commit_unix_time = time_str.unwrap().parse::<u64>().or(Err(String::from("unix time should be parsable as u64...")))?;

    let start = SystemTime::now();
    let since_the_epoch_duration = start.duration_since(UNIX_EPOCH).or(Err(String::from("Time went backwards")))?;
    let since_the_epoch =since_the_epoch_duration.as_secs();

    let max_days: u64 = conf.clean_git_days * 60 * 60 * 24;
    if since_the_epoch - first_commit_unix_time > max_days {
        git_clean_history()?;
    }

    Ok(())
}

fn main() {
    env_logger::init();
    let mut conf_own = Conf::open().expect("Cannot open config file!");
    conf_own.base_directory = Some(format!("{}", std::env::current_dir().expect("Cannot obtain $CWD").display()));
    let conf = Rc::new(conf_own);

    cd_to_git_folder(conf.clone()).expect("git initialization failed");
    try_clean_git(conf.clone()).expect("Checking to clean git");
    
    let mut podcasts = String::from("[");
    let mut used_audio_files: Vec<String> = vec!();

    for feed in conf.rss_urls.iter() {
        let channel = Channel::from_url(feed).unwrap();
        let small_channel = SmallChannel::from_rss(&channel, conf.clone());

        for item in &small_channel.items {
            if !item.is_transcoded() {
                if !item.is_downloaded() {
                    info!("Downloading {} -> {}", item.title, item.temporary_file());
                    item.download_file();
                } else {
                    info!("Already downloaded: {} -> {}", item.title, item.temporary_file());
                }
                info!("Transcoding: {} -> {}", item.title, item.transcoded_file());
                item.transcode().unwrap();
            } else {
                info!("Already transcoded: {} -> {}", item.title, item.transcoded_file());
            }

            used_audio_files.push(item.transcoded_file());
        }

        small_channel.write_to_disk();

        let jc = JsonChannel::from(small_channel);
        let separator = if podcasts.len() > 1 { ", " } else { "" };
        let jsc = serde_json::to_string(&jc).unwrap();
        podcasts = format!("{}{}{}", podcasts, separator, jsc);
    }

    clean_unused_audio_files(&used_audio_files).unwrap();

    run_command(&format!("rm -r {}", TEMPORARY_AUDIO_FOLDER)).expect("Can not remove temporary audio files");
    run_command(&format!("mkdir {}", TEMPORARY_AUDIO_FOLDER)).expect("Can not recreate folder for temporary audio files");

    let mut file = fs::File::create("podcasts.json").expect("Could not podcasts.json");
    file.write_all(podcasts.as_bytes()).unwrap();
    file.write_all("]".as_bytes()).unwrap();

    if let Err(e) = git(conf.clone()) {
        warn!("Uploaded error: {}", e);
    }
}
