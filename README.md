# Smallpodcasts

This projects enables you, to create much smaller versions of podcasts, that you are listening to. To do this, we use a couple of nice technologies:

* opus audio codec, which makes perfectly good sounding podcasts at very low bitrates,
* github for hosting,
* statically for content delivery,
* oopml for listing the rss feeds and importing them into apps

What this app does:

* takes a list of rss podcast feeds
* generates smaller feeds with less items with less data (some rss feeds are bigger than a MB)
* takes enclosed audio files, converts them to smalle opus files
* uploads that to github and makes it publically available

What you should not do:

* Beware of the size of the repo, if you go over a GB, the github will probably send you a letter
* Beware of the bandwidth, if too many people use the feed, statically will probably send you a letter
* Beware of licences, many podcast prohibit any modifications, so they also might not be happy about this

## Opus codec

Opus codec is new audio codec supported in Android 5 and Iphone X and later.
To show, why it is used here, I present a few audio samples.

* [MP3 128kbps](https://cdn.staticaly.com/gl/gareins/smallpodcasts/master/static/small-podcasts-master/static/bv128.mp3)
* [Opus 64kbps](https://cdn.staticaly.com/gl/gareins/smallpodcasts/master/static/small-podcasts-master/static/bv64.ogg)
* [Opus 32kbps](https://cdn.staticaly.com/gl/gareins/smallpodcasts/master/static/small-podcasts-master/static/bv32.ogg)
* [Opus 24kbps](https://cdn.staticaly.com/gl/gareins/smallpodcasts/master/static/small-podcasts-master/static/bv24.ogg)
* [Opus 16kbps](https://cdn.staticaly.com/gl/gareins/smallpodcasts/master/static/small-podcasts-master/static/bv16.ogg)

By default we use 32kbps, but this will be configurable soon.

## Usage

I only use this under Linux, so I give you instructions for most distros. This should
probably also work under osx quite easily. Ok, so you first need to compile the code. 
This is written in rust, so you should probably use rustup to get the rustc and cargo.
Then run

`cargo build`

You will also need ffmpeg and git installed, as executables.
Next, you need to create a blank repository on github. You should probably enable github pages on this repo.
You will need to have ssh push to that repository.
Then, create your config by copying *static/conf.toml* to *conf.toml*
and edit it to your liking - github username and repository must match.

Then create an empty directory that matches the path to *git_repo_path* in conf.toml.
Now, you can initialize and run the application for the first time.

`cargo run`

So now, the repo is hopefully initialized, index.html is on master branch
and rss feeds and opus files are on audio branch. To update everything just do a rerun.

To get the feeds, go to whatever url the github pages feature of your github gave you and you should see something like the image below.
Here, the right button generates the oopml file, that contains links to feeds. Now just use podcast player, that can update this file
and you are good to go.

![Example webpage](static/html.jpeg)
